import math
import numpy

from robots.mathbot import Mathbot
from controllers.params import Controller

class FollowWall(Controller):
    """Follow walls is a controller that keeps a certain distance
    to the wall and drives alongside it in clockwise or counter-clockwise
    fashion."""

    def __init__(self, params,robot):
        '''Initialize internal variables'''
        Controller.__init__(self,'fw')
        self.params = params
        self.robot: Mathbot = robot
        self.e_old = 0
        self.E = 0
        # This variable should contain a list of vectors
        # calculated from the relevant sensor readings.
        # It is used by the supervisor to draw & debug
        # the controller's behaviour
        self.sensor_states_r = [sensor.state_r for sensor in self.robot.sensors]
        self.sensor_weights = [(math.cos(state.phi + 1.5)) for state in self.sensor_states_r]
        ws = sum(self.sensor_weights)
        #self.sensor_weights = [w / ws for w in self.sensor_weights]
        self.sensor_weights = [1, 1, 0.5, 1, 1]
        ##self.sensor_weights = [0.5, 0.75, 1.0, 0.75, 0.5]
        self.vectors = None
        
    def restart(self):
        """Reset internal state"""
        self.e_old = 0
        self.E = 0
        
        # This vector points to the closest point of the wall
        self.to_wall_vector = None
        
        # This vector points along the wall
        self.along_wall_vector = None
        
        # Both vectors are None to enable smoother corner navigation

    def set_parameters(self, params):
        """Set PID values, sensor poses, direction and distance.
        
        The params structure is expected to have sensor poses in the robot's
        reference frame as ``params.sensor_poses``, the direction of wall
        following (either 'right' for clockwise or 'left' for anticlockwise)
        as ``params.direction``, the desired distance to the wall 
        to maintain as ``params.distance``, and the maximum sensor reading
        as ``params.ir_max``.
        """
        #PIDController.set_parameters(self,params)

        self.direction = params['direction']
        self.distance = params['distance']
        self.sensor_max = params['ir_max']

    def get_heading(self, state, sensor_distances):
        """Get the direction along the wall as a vector."""
        
        # Factor for sensor selection
        if self.direction == 'left':
            dirfactor = 1
        else:
            dirfactor = -1
        
        # Get the sensors at the good side that also show an obstacle
        sensors = [(p, d) for d, p in zip(sensor_distances, self.sensor_states_r)
                          if 0 < p.phi*dirfactor < math.pi and d < self.sensor_max]

        # Now make sure they are sorted from front to back
        for index in range(1,len(sensors)):
            currentvalue = sensors[index]
            position = index

            while position>0 and sensors[position-1][0].phi>currentvalue[0].phi:
                sensors[position]=sensors[position-1]
                position = position-1

            sensors[position]=currentvalue
        
        # No wall - drive a bit to the wall
        if len(sensors) == 0:
            return numpy.array([0.8,dirfactor*0.6,1])
        
        # Calculate vectors for the sensors 
        self.vectors = numpy.array(
                            [numpy.dot(p.get_transformation(),
                                    numpy.array([d,0,1]))
                             for p, d in sensors] )

        # Now the wall section: (really easy version)
        if len(self.vectors) == 1: # Corner
            sensor = sensors[0]
            pose = sensor[0]
            reading = sensor[1]
            self.to_wall_vector = self.vectors[0]
            if self.along_wall_vector is None:
                # We've only started, it's a corner,
                # go perpendicular to its vector
                self.along_wall_vector = numpy.array([
                            dirfactor*self.to_wall_vector[1],
                            -dirfactor*self.to_wall_vector[0],
                            1])

                # Which direction to go?
                # either away from this corner or directly to it.
                # let's blend ahead with corner:
                theta_h = pose.phi*reading/self.sensor_max
                return numpy.array([
                            reading*math.cos(theta_h),
                            reading*math.sin(theta_h),
                            1])
            else:
                # To minimize jittering, blend with the previous
                # reading, and don't rotate more than 0.2 rad.
                prev_theta = math.atan2(self.along_wall_vector[1],
                                        self.along_wall_vector[0])
                self.along_wall_vector = numpy.array([
                            dirfactor*self.to_wall_vector[1],
                            -dirfactor*self.to_wall_vector[0],
                            1])
                this_theta = math.atan2(self.along_wall_vector[1],
                                        self.along_wall_vector[0])
                dtheta = prev_theta - this_theta
                if abs(dtheta) > 0.2:
                    dtheta *= 0.2*abs(dtheta)
                
                self.along_wall_vector = numpy.array([
                            reading*math.cos(prev_theta - dtheta),
                            reading*math.sin(prev_theta - dtheta),
                            1])
                                    
                
        else: # More than one vector, approximate with first and last
            self.along_wall_vector = self.vectors[0] - self.vectors[-1]
            a = self.vectors[-1]
            b = self.along_wall_vector
            dot = numpy.dot
            self.to_wall_vector = a - b*dot(a,b)/dot(b,b)

        # Blend along_wall with to_wall depending on the distance
        # if too small, go further away (-to_wall), if too big, go closer
        # (+ to_wall), or go along.
    
        weight_steer = math.sqrt(self.to_wall_vector[0]**2
                            + self.to_wall_vector[1]**2) - self.distance
        weight_steer /= self.sensor_max/2
        
        # Extra weight for driving closer to corners
        if len(self.vectors) == 1:
            weight_steer += 0.3
                           
        return self.along_wall_vector + self.to_wall_vector*weight_steer
               
    def execute(self, state, robot, sensor_distances, dt):
        heading = self.get_heading(state, sensor_distances)
        heading_angle = math.atan2(heading[1], heading[0])

        e_p = heading_angle
        e_i = self.E + e_p * dt
        e_d = (e_p - self.e_old) / dt

        w = self.params.k_p*e_p + self.params.k_i*e_i + self.params.k_d*e_d
        v = robot.specs.v_max

        self.e_old = e_p
        self.E = e_i

        return v, w